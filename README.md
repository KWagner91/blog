# BLOG WEBSITE :pencil:

A Blog Website build with Java, Spring Bot, HeidiSQL and Thymeleaf. All Blog entries are saved on a data base, Users can create accounts and write comments to blog articles when logged in. Only Administrators can write new blog articles and give administration rights to users.

## Getting Started

For this project you need to download the files and unzip them.

## Project Link Structure

```
/
|
|---blog/
|   |
|   |---/blog/comments/{id}
|
|---login/
|   |
|   |---loginSuccess
|   
|---register/
|   |   
|   |---register/success
|   
|---admin/
|   |
|   |---admin/{userid}
```


### Prerequisites

What things you need to install the software and how to install them

* Java
* Maven
* MariaDB
* Git
* a Java Editor (e.g IntelliJ, Eclipse)

### Installing

```
git clone https://gitlab.com/KWagner91/blog/
cd blog
```


## Built With

* [Java](https://www.oracle.com/technetwork/java/javase/downloads/index.html) - The Programming Language used
* [Maven](https://maven.apache.org/) - Software Project Management Tool
* [Spring Web](https://mvnrepository.com/artifact/org.springframework/spring-web) - Creating Web Services
* [Spring-Boot](https://spring.io/projects/spring-boot) - Create Stand-alone Spring applications
* [Thymeleaf](https://www.thymeleaf.org/) - Template Engine for HTML templates
* [HTML5](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5) - Text Entries and Forms
* [CSS3](https://developer.mozilla.org/en-US/docs/Glossary/CSS) - Styles where created with Vanilla CSS
* [Git](https://git-scm.com/) - Open Source Version Control System


## Authors

* **Kerstin Bonev-Wagner** - *Initial work* - [Gitlab](https://gitlab.com/KWagner91)
* **Adam Korkosz** - *Initial work* - [Gitlab](https://gitlab.com/adakor)



## User Stories (Project Requirements)
1. Als Nutzer kann ich die Beiträge des Blogs in chronologischer Reihenfolge (neueste zuerst) lesen, um Einblick in die Gedanken der Administratoren zu erhalten. :heavy_check_mark:
2. Als Nutzer kann ich die Kommentare zu einem Beitrag in chronologischer Reihenfolge (älteste zuerst) lesen, um Einblick in die Meinungen der Nutzer zu erhalten. :heavy_check_mark:
3. Als anonymer Nutzer kann ich mich mit Benutzername und Passwort registrieren, damit meine Identität etabliert wird. :heavy_check_mark:
4. Als anonymer Nutzer kann ich mich mit vorher registrierten Benutzername und Passwort einloggen, um registrierter Nutzer zu werden. :heavy_check_mark:
5. Als registrierter Nutzer kann ich Kommentare zu einzelnen Beiträgen schreiben um mit den Administratoren und anderen Nutzern meine Meinung zum Thema des Beitrags zu teilen. :heavy_check_mark:
6. Als Administrator kann ich registrierte Nutzer zu Administratoren benennen, damit sie mich bei bei Redaktion und
 Verwaltung des Blogs unterstützen können. :heavy_check_mark:
7. Als Administrator kann ich neue Beiträge in dem Blog erstellen, um meine Gedanken mit der Welt zu teilen. Beiträge haben eine Überschrift und einen Inhalt (beides nur Text). :heavy_check_mark:
8. Als registrierter Nutzer kann ich von mir erstellte Kommentare löschen, um meiner geänderten Meinung gerecht zu werden.
9. Als Administrator kann ich Kommentare löschen, um die Qualität des Inhalts meines Blogs sicher zu stellen.
10. Als Administrator kann ich Beiträge bearbeiten, um Fehler auszubessern.
11. Als Administrator kann ich Beiträge inklusiver der zugehörigen Kommentare löschen, um grobe
Fehler zu korrigieren
