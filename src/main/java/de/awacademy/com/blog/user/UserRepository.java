package de.awacademy.com.blog.user;

import org.springframework.data.repository.CrudRepository;
import java.util.Optional;
import java.util.List;


public interface UserRepository extends CrudRepository<User, Integer> {
    List<User> findAll();
    Optional<User> findByUserName(String userName);
}
