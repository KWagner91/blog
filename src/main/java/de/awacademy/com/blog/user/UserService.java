package de.awacademy.com.blog.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    private UserDTO userDTO;
    @OneToMany
    private List<User> users;
    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public User getUserbyId(Integer id) {
        Optional<User> user = userRepository.findById(id);
        return user.orElse(null);
    }

    public void saveUser(User user) {
        userRepository.save(user);
    }

    public Optional<User> getUser(String userName) {
        Optional<User> optionalDataBaseUser = userRepository.findByUserName(userName);
        return optionalDataBaseUser;
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void changeAdminStatus(User user) {
        userRepository.save(user);
    }
}

