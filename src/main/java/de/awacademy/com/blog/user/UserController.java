package de.awacademy.com.blog.user;

import de.awacademy.com.blog.session.Session;
import de.awacademy.com.blog.session.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.Optional;

@Controller
public class UserController {

    private UserService userService;
    private SessionRepository sessionRepository;

    @Autowired
    public UserController(UserService userService, SessionRepository sessionRepository) {
        this.userService = userService;
        this.sessionRepository = sessionRepository;
    }

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("userDTO", new UserDTO());
        return "loginSuccess";
    }

    @GetMapping("/deleteCookie")
    public String login(HttpServletResponse httpServletResponse) {

        Cookie cookie = new Cookie("sessionId", "");
        httpServletResponse.addCookie(cookie);
        return "redirect:/blog";
    }

    /* Create Session and Coookie when user logs in */
    @PostMapping("/login")
    public String login(@ModelAttribute("userDTO") UserDTO userDTO, Model model, HttpServletResponse httpServletResponse) {

        Optional<User> dataBaseUser = userService.getUser(userDTO.getUserName());

        if (dataBaseUser.isPresent()) {

            if (userDTO.getPassword().equals(dataBaseUser.get().getPassword())) {
                Session session = new Session(dataBaseUser.get(), Instant.now().plusSeconds(60*60));
                sessionRepository.save(session);
                Cookie cookie = new Cookie("sessionId", session.getId());
                httpServletResponse.addCookie(cookie);
                model.addAttribute("userName", userDTO.getUserName());
                return "loginSuccess";
            }
        }
        return "index";
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("userDTO", new UserDTO());
        return "register";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute("userDTO") UserDTO userDTO, HttpServletResponse httpServletResponse) {

        Cookie cookie = new Cookie("sessionId", "");
        httpServletResponse.addCookie(cookie);

        userService.saveUser(new User(userDTO.getUserName(), userDTO.getPassword()));
        return "loginSuccess";
    }

    @PostMapping("/register/success")
    public String registerSuccess(@ModelAttribute("userDTO") UserDTO userDTO, Model model, HttpServletResponse httpServletResponse) {

        Cookie cookie = new Cookie("sessionId", "");
        httpServletResponse.addCookie(cookie);

        userService.saveUser(new User(userDTO.getUserName(), userDTO.getPassword()));
        model.addAttribute("userName", userDTO.getUserName());

        return "registerSuccess";
    }

    @GetMapping("/admin")
    public String admin(@ModelAttribute("sessionUser") User sessionUser, Model model) {

        if (sessionUser == null) {
            return "admin";
        }

        model.addAttribute("users", userService.getUsers());
        model.addAttribute("sessionUser", sessionUser);
        return "admin";
    }

    @GetMapping("/admin/{userid}")
    public String admin(@PathVariable Integer userid, Model model) {

        User userChangedAdminStatus = userService.getUserbyId(userid);
        userChangedAdminStatus.changeAdminStatus();
        userService.saveUser(userChangedAdminStatus);
        return "redirect:/admin";
    }
}
