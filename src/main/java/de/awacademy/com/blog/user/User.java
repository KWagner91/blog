package de.awacademy.com.blog.user;

import de.awacademy.com.blog.blogentry.BlogEntry;
import de.awacademy.com.blog.comment.Comment;
import javax.persistence.*;
import java.util.List;


@Entity
public class User {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;
    private String userName;
    private String password;
    private boolean isAdmin;
    @OneToMany(mappedBy = "user")
    private List<BlogEntry> blogEntry;
    @OneToMany(mappedBy = "user")
    private List<Comment> comments;

    public User() {
    }

    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
        this.isAdmin = false;
    }

    public Integer getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void changeAdminStatus() {
        this.isAdmin = !this.isAdmin;
    }
}


