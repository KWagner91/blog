package de.awacademy.com.blog.comment;

import de.awacademy.com.blog.blogentry.BlogEntryService;
import de.awacademy.com.blog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class CommentController {

    private CommentService commentService;
    private BlogEntryService blogEntryService;

    @Autowired
    public CommentController(CommentService commentService, BlogEntryService blogEntryService) {

        this.blogEntryService = blogEntryService;
        this.commentService = commentService;
    }

    @GetMapping("/blog/comments/{id}")
    public String idComment(@PathVariable Integer id, Model model) {

        model.addAttribute("comments", commentService.getComments(id));
        model.addAttribute("commentDTO", new CommentDTO("",""));
        model.addAttribute("blogEntry", blogEntryService.getBlogEntry(id));
        return "blogComments";
    }

    @PostMapping("/blog/comments/{id}")
    public String readComment(@PathVariable Integer id, @ModelAttribute CommentDTO commentDTO, @ModelAttribute(
            "sessionUser") User sessionUser, Model model) {

        if (sessionUser.getUserName() != null) {
            commentService.addComment(new Comment(commentDTO.getHeader(), commentDTO.getText(), blogEntryService.getBlogEntry(id), sessionUser));
            model.addAttribute(new CommentDTO());
            model.addAttribute("comments", commentService.getComments(id));
            return "redirect:/blog/comments/" + id;
        }
        return "redirect:/blog/comments/" + id;
    }

    @PostMapping("/blog/comments/{blogEntryid}/{commentId}")
    public String deleteComment(@ModelAttribute("sessionUser") User sessionUser, @PathVariable Integer blogEntryid, @PathVariable Integer commentId, Model model) {

        Comment foundComment = commentService.findById(commentId);

        if ((sessionUser.getId() == foundComment.getUser().getId()) || (sessionUser.isAdmin())) {

            foundComment.getBlogEntry().decreaseCommentCounter();
            commentService.deleteComment(foundComment);
        }

        model.addAttribute("comments", commentService.getComments(commentId));
        return "redirect:/blog/comments/" + blogEntryid;
    }
}
