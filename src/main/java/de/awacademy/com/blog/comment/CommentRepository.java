package de.awacademy.com.blog.comment;

import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;


public interface CommentRepository extends CrudRepository<Comment, Integer> {

    Optional<Comment> findById(Integer id);
    List<Comment> findAll();
    List<Comment> findCommentsByBlogEntryId(Integer id);

    @Override
    void delete(Comment comment);
}
