package de.awacademy.com.blog.comment;

import de.awacademy.com.blog.blogentry.BlogEntry;
import de.awacademy.com.blog.user.User;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String header;
    private String text;
    private String dateTime;
    @ManyToOne
    private BlogEntry blogEntry;
    @ManyToOne
    private User user;


    public Comment(){
    }

    public Comment(String header, String text, BlogEntry blogEntry, User user) {
        this.header = header;
        this.text = text;
        this.blogEntry = blogEntry;
        this.user = user;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yy - HH:mm:ss");
        dateTime = dtf.format(LocalDateTime.now());
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public BlogEntry getBlogEntry(){
        return this.blogEntry;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
