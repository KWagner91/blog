package de.awacademy.com.blog.comment;

import javax.validation.constraints.NotEmpty;


public class CommentDTO {

    @NotEmpty
    private String header;
    @NotEmpty
    private String text;


    public CommentDTO(String header, String text) {
        this.header = header;
        this.text = text;
    }

    public CommentDTO() {
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
