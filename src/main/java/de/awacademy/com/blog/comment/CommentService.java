package de.awacademy.com.blog.comment;

import de.awacademy.com.blog.blogentry.BlogEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class CommentService {

    private CommentRepository commentRepository;
    private BlogEntryService blogEntryService;

    @Autowired
    public CommentService(CommentRepository commentRepository, BlogEntryService blogEntryService) {
        this.commentRepository = commentRepository;
        this.blogEntryService = blogEntryService;
    }

    public void addComment(Comment comment) {
        blogEntryService.getBlogEntry(comment.getBlogEntry().getId()).increaseCommentCounter();
        commentRepository.save(comment);
    }

    public List<Comment> getComments(int id) {
        return commentRepository.findCommentsByBlogEntryId(id);
    }

    public Comment findById(int id) {
        return commentRepository.findById(id).get();
    }

    public void deleteComment(Comment comment) {
        commentRepository.delete(comment);
    }

    public void deleteCommentsByBlogEntryId(Integer id) {

        List<Comment> foundComments = commentRepository.findCommentsByBlogEntryId(id);

        for (Comment comment: foundComments) {
            commentRepository.delete(comment);
        }
    }
}
