package de.awacademy.com.blog.blogentry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;


@Service
public class BlogEntryService {

    private BlogEntryDTO blogEntryDTO;
    private BlogEntryRepository blogEntryRepository;

    @Autowired
    public BlogEntryService(BlogEntryRepository blogEntryRepository) {
        this.blogEntryDTO = new BlogEntryDTO();
        this.blogEntryRepository = blogEntryRepository;
    }

    public void saveBlogEntry(BlogEntry blogEntry){
        blogEntryRepository.save(blogEntry);
    }

    public List<BlogEntry> getBlogEntries(){
        return blogEntryRepository.findAllByOrderByIdDesc();
    }

    public BlogEntryDTO getBlogEntryDTO() {
        return blogEntryDTO;
    }

    public BlogEntry getBlogEntry(int id) {

       Optional<BlogEntry> blog = blogEntryRepository.findById(id);
        return blog.get();
    }

    public void deleteBlogEntryById(Integer id) {

        BlogEntry foundBlogEntry = blogEntryRepository.findById(id).get();
        blogEntryRepository.delete(foundBlogEntry);
    }

    public void updateBlogEntry(Integer id, BlogEntryDTO blogEntryDTO) {

        BlogEntry foundBlogEntry = blogEntryRepository.findById(id).get();
        foundBlogEntry.setHeader(blogEntryDTO.getHeader());
        foundBlogEntry.setText(blogEntryDTO.getText());
        blogEntryRepository.save(foundBlogEntry);
    }
}
