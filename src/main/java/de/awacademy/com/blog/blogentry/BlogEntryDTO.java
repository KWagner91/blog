package de.awacademy.com.blog.blogentry;


public class BlogEntryDTO {

    private String header;
    private String text;

    public BlogEntryDTO(String header, String text) {
        this.header = header;
        this.text = text;
    }

    public BlogEntryDTO(){
    }

    public String getHeader() {
        return header;
    }

    public String getText() {
        return text;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setText(String text) {
        this.text = text;
    }
}

