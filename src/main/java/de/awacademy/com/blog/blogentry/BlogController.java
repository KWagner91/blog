package de.awacademy.com.blog.blogentry;

import de.awacademy.com.blog.comment.CommentService;
import de.awacademy.com.blog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;


@Controller
public class BlogController {

    private BlogEntryService blogEntryService;
    private CommentService commentService;

    @Autowired
    public BlogController(BlogEntryService blogEntryService, CommentService commentService) {
        this.blogEntryService = blogEntryService;
        this.commentService = commentService;
    }

    // Method @GetMapping("/") can be found in SessionController
    @PostMapping("/")
    public String login(Model model) {
        return "redirect:/blog";
    }

    @GetMapping("/blog")
    public String getBlogEntries(@ModelAttribute("sessionUser") User sessionUser, Model model) {

        model.addAttribute("blogEntryDTO", blogEntryService.getBlogEntryDTO());
        model.addAttribute("blogEntries", blogEntryService.getBlogEntries());
        model.addAttribute("sessionUser", sessionUser);
        return "blogEntries";
    }

    @PostMapping("/blog")
    public String blogSubmitted(@ModelAttribute("sessionUser") User sessionUser, @ModelAttribute("blogEntryDTO") BlogEntryDTO blogEntryDTO,
                               BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "blogEntries";
        }

        BlogEntry blogE= new BlogEntry(blogEntryDTO.getHeader(), blogEntryDTO.getText(), sessionUser);
        blogEntryService.saveBlogEntry(new BlogEntry(blogEntryDTO.getHeader(), blogEntryDTO.getText(), sessionUser));
        model.addAttribute("sessionUser", sessionUser);
        model.addAttribute(blogE);
        model.addAttribute("entries", blogEntryService.getBlogEntries());
        return "redirect:/blog";
    }

    @PostMapping("/blog/options/{blogEntryid}")
    public String adminOptions(@PathVariable Integer blogEntryid){

        BlogEntry foundBlogEntry = blogEntryService.getBlogEntry(blogEntryid);
        foundBlogEntry.setAdminOptionsVisible(!foundBlogEntry.isAdminOptionsVisible());
        blogEntryService.saveBlogEntry(foundBlogEntry);
        return"redirect:/blog";
    }

    @PostMapping("/blog/update/{blogEntryid}")
    public String updateBlogEntry(@PathVariable Integer blogEntryid, @ModelAttribute BlogEntryDTO blogEntryDTO, @ModelAttribute("sessionUser") User sessionUser) {

        blogEntryService.updateBlogEntry(blogEntryid, blogEntryDTO);
        return "redirect:/blog";
    }

    @PostMapping("/blog/delete/{blogEntryid}")
    public String deleteBlogEntry(@PathVariable Integer blogEntryid, @ModelAttribute("sessionUser") User sessionUser) {

        commentService.deleteCommentsByBlogEntryId(blogEntryid);
        blogEntryService.deleteBlogEntryById(blogEntryid);
        return "redirect:/blog";
    }
}


