package de.awacademy.com.blog.blogentry;

import de.awacademy.com.blog.user.User;
import de.awacademy.com.blog.comment.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.*;
import java.util.List;


@Entity
public class BlogEntry {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private User user;
    private String header;
    private String text;
    @OneToMany(mappedBy = "blogEntry")
    private List<Comment> comments;
    private int commentsCounter;
    private boolean adminOptionsVisible;


    public BlogEntry() {
    }

    @Autowired
    public BlogEntry(String header, String text, User user) {
        this.header = header;
        this.text = text;
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Integer getId() {
        return id;
    }

    public String getHeader() {
        return header;
    }

    public String getText() {
        return text;
    }

    public void increaseCommentCounter(){
        this.commentsCounter++;
    }

    public void decreaseCommentCounter(){
        this.commentsCounter--;
    }

    public int getCommentsCounter() {
        return commentsCounter;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isAdminOptionsVisible() {
        return adminOptionsVisible;
    }

    public void setAdminOptionsVisible(boolean adminOptionsVisible) {
        this.adminOptionsVisible = adminOptionsVisible;
    }
}

