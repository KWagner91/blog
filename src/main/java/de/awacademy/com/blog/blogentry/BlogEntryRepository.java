package de.awacademy.com.blog.blogentry;

import org.springframework.data.repository.CrudRepository;
import java.util.List;


public interface BlogEntryRepository extends CrudRepository<BlogEntry, Integer> {
    List<BlogEntry> findAll();
    List<BlogEntry> findAllByOrderByIdDesc();

    @Override
    void delete(BlogEntry blogEntry);
}
