package de.awacademy.com.blog.session;

import de.awacademy.com.blog.blogentry.BlogEntry;
import de.awacademy.com.blog.blogentry.BlogEntryService;
import de.awacademy.com.blog.user.User;
import de.awacademy.com.blog.user.UserDTO;
import de.awacademy.com.blog.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import javax.annotation.PostConstruct;


@Controller
public class SessionController {

    private SessionService sessionService;
    private UserRepository userRepository;
    private BlogEntryService blogEntryService;

    @Autowired
    public SessionController(SessionService sessionService, UserRepository userRepository, BlogEntryService blogEntryService) {
        this.sessionService = sessionService;
        this.userRepository = userRepository;
        this.blogEntryService = blogEntryService;
    }

    @GetMapping("/")
    public String login(Model model){
        model.addAttribute("userDTO", new UserDTO());
        return "index";
    }

    // Stellt automatisch einen neuen Admin nutzer und Testbeiträge bereit
    @PostConstruct
    public void prepareSampleData(){

        if (userRepository.count() == 0) {
            userRepository.save(new User("Adam", "Adam"));

            User userChangedAdminStatus = userRepository.findById(1).get();
            userChangedAdminStatus.changeAdminStatus();
            userRepository.save(userChangedAdminStatus);
            blogEntryService.saveBlogEntry(new BlogEntry("Das ist ein Auto Test Beitrag","Mit text roflcat text", userChangedAdminStatus));
            blogEntryService.saveBlogEntry(new BlogEntry("Auto Test Beitrag Nummer 2","Sehr viel Text hier", userChangedAdminStatus));
            blogEntryService.saveBlogEntry(new BlogEntry("Nummer 3 ist am start","und spammt mich zu", userChangedAdminStatus));
            blogEntryService.saveBlogEntry(new BlogEntry("Die 4 wieder im Revier","war nie wirklich weg", userChangedAdminStatus));
            blogEntryService.saveBlogEntry(new BlogEntry("TODO: Irgendwo noch einbauen","dass bei Anlage eines neuen Users zunächst noch geprüft werden muss, ob es bereits einen User mit gleichem Namen gibt", userChangedAdminStatus));
        }
    }
}

