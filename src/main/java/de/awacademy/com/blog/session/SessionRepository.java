package de.awacademy.com.blog.session;

import de.awacademy.com.blog.user.User;
import org.springframework.data.repository.CrudRepository;
import java.time.Instant;
import java.util.Optional;


public interface SessionRepository extends CrudRepository<Session, Integer> {
    Optional<Session> findSessionByUser(User user);
    Optional<Session> findSessionById(String id);
    Optional<Session> findByIdAndExpiresAtAfter(String id, Instant instant);
}
