package de.awacademy.com.blog.session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.Instant;
import java.util.Optional;


@Service
public class SessionService {

    private SessionRepository sessionRepository;

    @Autowired
    public SessionService(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public Optional<Session> findSessionById(String id) {
        return sessionRepository.findSessionById(id);
    }

    public Optional<Session> findByIdAndExpiresAtAfter(String id, Instant instant) {
        return sessionRepository.findByIdAndExpiresAtAfter(id, instant);
    }
}
