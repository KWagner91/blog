package de.awacademy.com.blog.session;

import de.awacademy.com.blog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import java.time.Instant;
import java.util.Optional;


@ControllerAdvice
public class SessionControllerAdvice {

    private SessionService sessionService;
    private SessionRepository sessionRepository;

    @Autowired
    public SessionControllerAdvice(SessionService sessionService, SessionRepository sessionRepository) {
        this.sessionService = sessionService;
        this.sessionRepository = sessionRepository;
    }

    @ModelAttribute("sessionUser")
    public User sessionUser(@CookieValue(value="sessionId", defaultValue = "") String sessionId){

        if (!sessionId.isEmpty()) {

            Optional<Session> optionalSession = sessionService.findByIdAndExpiresAtAfter(sessionId, Instant.now());

            if(optionalSession.isPresent()) {
                Session session = optionalSession.get();
                session.setExpiresAt(Instant.now().plusSeconds(60*60));
                sessionRepository.save(session);

                return session.getUser();
            }
        }
        return null;
    }
}