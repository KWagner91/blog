package de.awacademy.com.blog.session;

import de.awacademy.com.blog.user.User;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;
import java.util.UUID;

@Entity
public class Session {

        @Id
        private String id = UUID.randomUUID().toString();
        @ManyToOne
        private User user;
        private Instant expiresAt;

        public Session() {
        }

        public Session(User user, Instant expiresAt) {
                this.user = user;
                this.expiresAt = expiresAt;
        }

        public String getId() {
                return id;
        }

        public void setId(String id) {
                this.id = id;
        }

        public User getUser() {
                return user;
        }

        public void setUser(User user) {
                this.user = user;
        }

        public Instant getExpiresAt() {
                return expiresAt;
        }

        public void setExpiresAt(Instant expiresAt) {
                this.expiresAt = expiresAt;
        }
}


